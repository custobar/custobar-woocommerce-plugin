<?php

/**
 * Plugin name: woocommerce-custobar
 * Description: Syncs relevant WooCommerce data to Custobar CRM.
 * Author: Sofokus
 * Text Domain: woocommerce-custobar
 * Domain Path: /languages
 * WC requires at least: 3.0
 */

require_once(__DIR__ . '/vendor/autoload.php');
require_once(__DIR__ . '/includes/functions.php');

\Sofokus\WooCommerceCustobar\load_localizations();

register_activation_hook(__FILE__, [\Sofokus\WooCommerceCustobar\Plugin::class, 'activate']);
register_deactivation_hook(__FILE__, [\Sofokus\WooCommerceCustobar\Plugin::class, 'deactivate']);

\Sofokus\WooCommerceCustobar\plugin()->initialize();
