# Contributing

## For code

1.  Inform related people about the wanted change, for bigger changes
    try to gather some opinions first
2.  Create an issue at the issue tracker, or pick one if something is
    available
3.  Clone this repository
4.  Do _not_ run `setup` when developing this base
5.  If the change is big, try to work on a feature branch first
6.  Create changes and fixes as reported in an issue
7.  Make sure the base works with your changes
8.  Document your changes, create tests if needed
9.  Commit and push to origin.

## For other things

Other things can be documentation, tooling choices, responsibilities and
so on. These should be initiated with discussion between those who use
and developer with this base.

If you want to enhance the documentation, feel free to do so at any
time.
